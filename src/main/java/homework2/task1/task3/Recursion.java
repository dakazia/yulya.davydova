package homework2.task1.task3;

public class Recursion {
    //Цикл бесконечный, для вывода одного значени нужно хранить в цикле два числа:
    //предыдущий результат и текущий. И каждый раз их сравнивать.
    //в какой-то момент текущий результат станет отрицательным
    //это знак что цикл нужно заканчивать.
    //Когда текущий результат становится меньше предыдущего, то значит значение перешло границу Integer.Max_VALUE.
    public static void main(String[] args) {
        int i = 1;
        int previousResult = 0;
        int currentResult = 0;
        try{
            while (true) {
                currentResult = fib(i++);
                if(currentResult < previousResult)
                    break;
                previousResult = currentResult;
            }
        } catch(StackOverflowError e){
        }
        System.out.println(previousResult);
    }
    public static int fib_r (int a, int b, int n) {
        if (n == 1) return a;
        if (n == 2) return b;
        return fib_r(b, a+b, n-1);
    }

    public static int fib (int n) {
        return fib_r(1, 1, n);
    }

}
