package homework2.task1.task2;

public class OrderOfPriority {
        static int staticVariable;
        int nonStaticVariable;

        static {
            System.out.println("Static initialization block.");
            staticVariable = 10;
        }
        static{
            System.out.println("The second static initialization block.");
        }
        {
            System.out.println("Instance initialization block.");
            nonStaticVariable = 20;
        }

        {
            System.out.println("The second instance initialization block.");
        }
        public OrderOfPriority () {
            System.out.println("Method main.");
        }

        public static void main(String []args){
            OrderOfPriority obj = new OrderOfPriority();
        }
// The output order will be: static initialization blocks, instance initialization blocks and then method main.

}
