package com.yd.training.homework1.task1.task2;

public class Algorithm {
    public static void main(String[] args) {
        int algorithmld, loopType, n;
        algorithmld = Integer.parseInt(args[0]);
        loopType = Integer.parseInt(args[1]);
        n = Integer.parseInt(args[2]);

        if (algorithmld == 1)
            printFibonacci( loopType, n);
        else if (algorithmld == 2)
            calculateFactorial( loopType, n);
        else System.out.println("Invalid input");
    }

    static void printFibonacci(int loopType, int n) {
        if (loopType == 1) {
            int num1 = 1, num2 = 1;
            int i = 1;
            while (i <= n) {
                System.out.print(num1 + " ");
                int sumOfPrevTwo = num1 + num2;
                num1 = num2;
                num2 = sumOfPrevTwo;
                i++;
            }
        } else if (loopType == 2) {
            int num1 = 1;
            int num2 = 1;
            int i = 1;
            System.out.print(num1 + " " + num2 + " ");
            do {
                num1 = num2;
                num2 = i;
                i = num1 + num2;
                System.out.print(i + " ");
            } while (i < n);
            System.out.println();
        }
        else if (loopType == 3) {
            int num1 = 1;
            int num2 = 1;
            int sumOfPrevTwo;
            System.out.print(num1 + " " + num2 + " ");
            for (int i = 3; i <= n; i++) {
                sumOfPrevTwo = num1 + num2;
                System.out.print(sumOfPrevTwo + " ");
                num1 = num2;
                num2 = sumOfPrevTwo;
            }
            System.out.println();

        } else System.out.println("Invalid input");
    }

    static void calculateFactorial(int loopType, int n) {
        if (loopType == 1) {
            long fact = 1;
            int i = 1;
            while (i <= n) {
                fact = fact * i;
                i++;
            }
            System.out.println("Factorial of " + n + " is: " + fact);
        } else if (loopType == 2) {
            long fact = 1;
            int i = 1;
            do {
                fact *= i;
                i++;
            } while (i <= n);
            System.out.println("Factorial of " + n + " is: " + fact);
        } else if (loopType == 3) {
            long fact = 1;
            for (int i = 1; i <= n; i++) {
                fact = fact * i;
            }
            System.out.println("Factorial of " + n + " is: " + fact);

        } else System.out.println("Invalid input");

    }

}
